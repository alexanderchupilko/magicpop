const head = jQuery('head');
const stylesLink = '<link rel="stylesheet" href="./magicPop/magicPopStyles.css">';
head.prepend(stylesLink);

class MagicPop{
    constructor(params) {
        this.type = params.type || 'info';
        this.parent = params.parent || 'body';
        this.buttonActivate = params.buttonActivate || null;
        this.backDropClose = typeof params.backDropClose === 'boolean' ? params.backDropClose : true;
        this.buttonCancel = typeof params.buttonCancel === 'boolean' ? params.buttonCancel : true;
        this.callbackOk =
            params.callbackOk ||
            function() {
                return true
            };
        this.callbackCancel =
            params.callbackCancel ||
            function() {
                return false
            };
        this.callbackClose =
            params.callbackClose ||
            params.callbackCancel ||
            function() {
                return null
            };
        this.animationShow = params.animationShow || null;
        this.animationHide = params.animationHide || null;
        this.animationShowDuration = params.animationShowDuration || 0;
        this.animationHideDuration = params.animationHideDuration || 0;
        this.buttonOkText = params.buttonOkText || 'Ok';
        this.buttonCancelText = params.buttonCancelText || 'Cancel';
        this.popupTitle = params.popupTitle || 'MagicPop';
        this.popupText =
            params.popupText ||
            `This text may be changed by 'popupText' parameter in params:<br>new MagicPop({popupText: 'sometext'})`;
        this._indexTags = [
            'a[href]',
            'area[href]',
            'input:not([disabled]):not([type="hidden"]):not([aria-hidden])',
            'select:not([disabled]):not([aria-hidden])',
            'textarea:not([disabled]):not([aria-hidden])',
            'button:not([disabled]):not([aria-hidden])',
            'iframe',
            'object',
            'embed',
            '[contenteditable]',
            '[tabindex]:not([tabindex^="-"])'
        ];
        this._popTypesClasses = {
            info: '',
            error: 'mp-error',
            success: 'mp-success'
        }
    }

    createMagicPop(){
        const popTypeClass = this._popTypesClasses[this.type] ? this._popTypesClasses[this.type] : '';
        const animation = this.animationShow ? this.animationShow : '';
        const addClasses = `${popTypeClass ? ` ${popTypeClass}` : ''}${animation ? ` ${animation}` : ''}`;
        this.magicPop = jQuery('<div>', {
            class: `magicPop${addClasses}`,
            attr: {'aria-hidden': 'true'},
            html: '<div class="mp-backdrop"></div>'
        });
        this.magicPopWrapper = jQuery('<div>', {
            class: 'mp-wrapper',
            attr: {'role': 'dialog', 'aria-modal': 'true'},
            html:
            `<div class="mp-header">
                <p class="mp-title">${this.popupTitle}</p>
                <button class="mp-close">x</button>
            </div>
            <p class="mp-message">${this.popupText}</p>
            <div class="mp-btns">
                <button class="mp-btn accept">${this.buttonOkText}</button>
                ${
                    this.buttonCancel
                        ? `<button class="mp-btn cancel">${this.buttonCancelText}</button>`
                        : ''
                }
            </div>`
        });
        jQuery(this.magicPopWrapper).appendTo(jQuery(this.magicPop).children()[0]);
    }
    init(){
        this.createMagicPop();
        if (this.buttonActivate) {
            jQuery(this.buttonActivate).on('click', () => {
                this.create();
                return false
            });
        }
    }
    create() {
        jQuery(this.parent).append(this.magicPop);
        this.addEvents();
        this.formFocusEls = jQuery(this.magicPop).find(this._indexTags.join(', '));
        this._focusItem = 0;
        jQuery(this.formFocusEls[0]).trigger('focus');
        if (this.animationShow) {
            jQuery(this.magicPopWrapper).css({'transition': `${this.animationShow} ${this.animationShowDuration}ms `});
            this.magicPop.removeClass(this.animationShow);
        }
    }
    remove() {
        jQuery(this.keydown).off();
        if (this.animationHide) {
            jQuery(this.magicPopWrapper).css({'transition': `${this.animationHide} ${this.animationHideDuration}ms`});
            jQuery(this.magicPop).addClass(this.animationHide);
            setTimeout(() => {
                jQuery(this.magicPop).remove();
                jQuery(this.magicPop).addClass(this.animationShow);
                jQuery(this.magicPop).removeClass(this.animationHide);
            }, this.animationHideDuration)
        } else {
            jQuery(this.magicPop).remove();
            jQuery(this.magicPop).addClass(this.animationShow);
        }

    }
    catchFocus(shiftDown){
        if (shiftDown) {
            this._focusItem--;
            if (this._focusItem < 0) {
                this._focusItem = this.formFocusEls.length - 1;
            }
        } else {
            this._focusItem++;
            if (this._focusItem >= this.formFocusEls.length) {
                this._focusItem = 0;
            }
        }
        jQuery(this.formFocusEls[this._focusItem]).trigger('focus');
    }
    addEvents() {
        jQuery(this.magicPop).one('click',(event) => {
            const target = event.target;
            if (jQuery(target).hasClass('mp-close')) {
                this.remove();
                this.callbackClose();
            } else if (jQuery(target).hasClass('mp-backdrop') && this.backDropClose) {
                this.remove();
                this.callbackClose();
            } else if (jQuery(target).hasClass('accept')) {
                this.remove();
                this.callbackOk();
            } else if (jQuery(target).hasClass('cancel')) {
                this.remove();
                this.callbackCancel();
            }
            return false
        });
        this.keydown = jQuery(document).on('keydown', (event) => {
            if (event.key === 'Escape') {
                this.remove();
            } else if (event.key === 'Tab') {
                this.catchFocus(event.shiftKey);
                return false;
            }
        });
    }
}

class TimeoutMagicPop extends MagicPop {
    constructor(params) {
        super(params);
        this.timeout = params.timeout || 10000;
        this.localStorageProp = params.localStorageProp || 'mpShown';
        this.showOnce = typeof params.showOnce === 'boolean' ? params.showOnce : true;
    }
    init() {
        super.init();
        if (this.timeout) {
            if (this.showOnce) {
                if (!this.isShown()) {
                    this.setShown();
                    setTimeout(() => {
                        this.setShown();
                        this.create();
                    }, this.timeout)
                }
            } else {
                setTimeout(() => this.create(), this.timeout);
            }
        }
    }
    isShown() {
        return localStorage.getItem(this.localStorageProp);
    }
    setShown() {
        localStorage.setItem(this.localStorageProp, 'true');
    }
}

class DeleterMagicPop extends MagicPop{
    constructor(params) {
        super(params);
        this.eventWrapperSelector = params.eventWrapperSelector || 'body';
        this.deleteElementSelector = params.deleteElementSelector;
        this.callbackOk = () => this.deleteFather(this.deleteBtn);
    }
    init() {
        this.createMagicPop();
        if (this.buttonActivate && this.deleteElementSelector) {
            jQuery(this.eventWrapperSelector).on('click', (event) => {
                if (jQuery(event.target).hasClass(this.buttonActivate.slice(1))) {
                    this.deleteBtn = event.target.closest(this.buttonActivate);
                    if (this.deleteBtn) {
                        this.create();
                    }
                }
                return false
            });
        }
    }
    deleteFather(deleteBtn) {
        const father = deleteBtn.closest(this.deleteElementSelector);
        jQuery(father).remove();
    }
}






