Hello, this is MagicPop.
MagicPop is a simple and configurable yet functional modal window that can:
    - remove the parent element when clicking on its child element;
    - appear by timeout once or constantly;
    - run when you click on any element;
    - configure the callback function for any choice in the modal window.


First, unzip the MagicPop folder to the root folder of the site!

Attention, this plugin uses jQuery version 3.6.3.

To find out if your site has jQuery, just go to the page where you want to use this plugin,
open the developer tools and enter the following in the console:

jQuery.fn.jquery

If you get something similar to "3.6.3" below,
then this is the version of jQuery that is already in use on your site.

In this case, you can try skip next paragraph)

If you get an error - just include our jQuery in the header using the tag:
<script defer src="magicPop/jquery.js"></script>

To connect this plugin, you need to connect the JavaScript file
(after connecting JQuery) using the tag:
<script defer src="magicPop/magicPop.js"></script>

To initialize this plugin just type the following in your javascript file:

For simple modal window:
new MagicPop({
    prop1: value1,
    prop2: value2,
    ...
}).init()

For timeout modal window:
new TimeoutMagicPop({
    prop1: value1,
    prop2: value2,
    ...
}).init()

For deleting modal window:
new DeleterMagicPop(
{
    prop1: value1,
    prop2: value2,
    ...
}).init()

Before starting, read about all the custom properties of this plugin.
prop: defaultVariant || otherVariant ....

MagicPop
    type: 'info' || 'warning' || 'error'                //visual theme of modal
    parent: 'body'                                      //first parent of modal (CSS selector)
    buttonActivate: null                                //click this element to show modal (only class selector as '.myBtn')
    backDropClose: true | false                         //modal closes when clicking on backdrop
    buttonCancel: true | false                          //show cancel button
    callbackOk:
        function() { return true }
        || other function what you want                 //callback function when clicking on OK (DO NOT TOUCH WITH DELETER USING)
    callbackCancel:
        function() { console.log('cancel') }
        || other function what you want                 //callback function when clicking on Cancel
    callbackClose:
        function() { console.log('close') }
        || other function what you want                 //callback function when modal closing
    buttonOkText: 'Ok'                                  //text in button 'OK'
    buttonCancelText: 'Cancel'                          //text in button 'Cancel'
    popupTitle: 'MagicPop'                              //title of modal
    popupText: 'Many letters'                           //text in modal
    animationShow:
        none                                            //show animation (none by default)
        || 'margin-bottom'
        || 'margin-left'
        || 'opacity'
        || 'scale',
    animationHide:
        none                                            //hide animation (none by default)
        || 'margin-bottom'
        || 'margin-left'
        || 'opacity'
        || 'scale'
    animationShowDuration: 0                            //duration of show animation (integer in millis)
    animationHideDuration: 0                            //duration of show animation (integer in millis)
}


TimeoutMagicPop extends MagicPop and have adding props
{
    timeout: 10000;                                     //show timeout after opening the page
    localStorageProp: 'mpShown'                         //type localStorage.removeItem('mpShown') in console to reset memory
    showOnce: true || false                             //show once for every client
}


DeleterMagicPop extends MagicPop and have adding props
{
    eventWrapperSelector: 'body'                        //single wrapper for all deleted parent elements (CSS selector)
    deleteElementSelector: '.father'                    //selector of all deleted elements (only class or id as '.father')
                                                            !!! specify at initialization

    buttonActivate: none                                //click this element to show modal and delete father element
                                                            !!! specify at initialization

    callbackOk             !!! don't touch this prop if you don't want crash plugin
}




